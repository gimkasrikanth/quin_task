//
//  SavedListView.swift
//  QuinTask
//
//  Created by Ali Almorshed on 14/04/21.
//

import SwiftUI
import MapKit

struct SavedListView: View {
    @Environment(\.safeAreaInsets) private var safeAreaInsets
    @State private var searchText: String = ""

    var body: some View {
        ZStack{
            Color.white
            if iHelper.locationsList.count > 0 {
                VStack{
                    Spacer()
                    SearchBar(placeholder: "Search Here", text: $searchText)
                    .padding([.top], 50)
                    ScrollView {
                        ForEach(iHelper.locationsList.filter({ (($0.title ?? "").contains(searchText)) || (($0.subTitle ?? "").contains(searchText))  || (searchText == "")}), id: \.self) { completion in
                                Button(action: {
                                    let destination = CLLocationCoordinate2D(latitude: completion.latitude, longitude: completion.longitude)
                                    let location = Location()
                                    location.title = completion.title
                                    location.subTitle = completion.subTitle
                                    location.destination = destination
                                    iHelper.selectedLocation = location
                                    UIApplication.shared.addRootController(view: AnyView(TabView(selectedTab: .home)))
                                }, label: {
                                    VStack(alignment: .leading) {
                                        Text(completion.title ?? "")
                                                .foregroundColor(Color.black)
                                                .padding([.leading], 5)
                                        Text(completion.subTitle ?? "")
                                            .foregroundColor(Color.black)
                                               .font(.subheadline)
                                            .padding([.leading], 5)
                                        Color.gray.frame(height:CGFloat(1) / UIScreen.main.scale)
                                   }
                                })
                        }
                        
                    }
                    .padding([.top], 10)
                }
                .padding(safeAreaInsets)
            }else{
                VStack(alignment: .center){
                    Text("No Data Available")
                }
            }

        }
        .edgesIgnoringSafeArea(.all)


    }
}

struct SavedListView_Previews: PreviewProvider {
    static var previews: some View {
        SavedListView()

    }
}
