//
//  TabView.swift
//  QuinTask
//
//  Created by Ali Almorshed on 14/04/21.
//

import SwiftUI
import CoreLocation

struct TabView: View {

    @State var selectedTab = TabName.home
    var edges = UIApplication.shared.windows.first?.safeAreaInsets
    @Namespace var animation
    @StateObject var modelData = ModelView()
    let tabBarTitles = ["Home", "Bookmarks"]
    let tabBarImages = ["homekit", "bookmark"]
    let tabBarNames: [TabName] = [.home, .saved]

    var body: some View{
        
        VStack(spacing: 0){
            
            GeometryReader{_ in
                
                ZStack{
                        

                    switch self.selectedTab{
                    case .home:
                        MapView(locationService: LocationService())

                    case .saved:
                        SavedListView()
                    }
                    VStack{
                        HStack{
                            Spacer()
                            Button {
                                UserDefaults.standard.setValue(nil, forKey: "Email")
                                UserDefaults.standard.setValue(nil, forKey: "Password")
                                UIApplication.shared.addRootController(view: AnyView(LoginView()))

                            } label: {
                                Text("Log out")
                            }
                            .frame(width: 100, height: 35)
                            .background(Color(UIColor(red: 223/255.0, green: 175/255.0, blue: 50/255.0, alpha: 1)))
                            .cornerRadius(5)
                            .padding([.trailing], 15)
                        }
                        Spacer()
                    }


                }
            }

            Color.gray.frame(height:CGFloat(1) / UIScreen.main.scale)
            HStack(spacing: 0){
                ForEach(0..<tabBarTitles.count){ i in
                    TabButton(id: tabBarNames[i], title: tabBarTitles[i], imageName: tabBarImages[i], selectedTab: $selectedTab, animation: animation, viewWidth: UIScreen.main.bounds.width/CGFloat(self.tabBarTitles.count))
                    
                    if self.tabBarTitles[i] != self.tabBarTitles.last{
                        Spacer(minLength: 0)
                    }
                }
            }
            .padding(.horizontal,0)
            .padding(.bottom,edges!.bottom == 0 ? 15 : edges!.bottom)
            .background(Color.white)
        }
        .ignoresSafeArea(.all, edges: .bottom)
        .background(Color.black.opacity(0.06).ignoresSafeArea(.all, edges: .all))
    }
}

struct TabView_Previews: PreviewProvider {
    static var previews: some View {
        TabView()
    }
}


