//
//  TabBarView.swift
//  QuinTask
//
//  Created by Ali Almorshed on 14/04/21.
//

import SwiftUI

struct TabBarView: View {
    var body: some View {

        // Type 1
        NavigationView{
            
            TabView(selectedTab: .home)
        }
    }
}

struct TabBarView_Previews: PreviewProvider {
    static var previews: some View {
        TabBarView()
    }
}
