//
//  LoginView.swift
//  QuinTask
//
//  Created by Ali Almorshed on 14/04/21.
//

import SwiftUI
import Combine

struct LoginView: View {
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var emailAlertText: String = ""
    @State private var passwordAlertText: String = ""
    @State var isTyping = false

    var body: some View {
        NavigationView {

            ZStack{
//                LinearGradient(gradient: Gradient(colors: [Color.blue, Color.white]), startPoint: .topLeading, endPoint: .bottomTrailing).edgesIgnoringSafeArea(.all)
//                Color.black.opacity(0.1).edgesIgnoringSafeArea(.all)
                Color(UIColor(red: 223/255.0, green: 175/255.0, blue: 50/255.0, alpha: 1)).edgesIgnoringSafeArea(.all)

                VStack{
                    Text("Welcome!").font(.system(size: 40, weight: .medium, design: .default))
                        .padding()
                        .foregroundColor(.white)
                    CustomTextField (placeholder: "Email", errorMessage: $emailAlertText, inputType: .email, editing: { (text, typing) in
                        if self.email != text{
                            self.emailAlertText = ""
                        }
                        self.email = text
                        self.isTyping = typing
                    })
                    CustomTextField (placeholder: "Password", errorMessage: $passwordAlertText, secureText: true, inputType: .text, editing: { (text, typing) in
                        if self.password != text{
                            self.passwordAlertText = ""
                        }
                        self.password = text
                        self.isTyping = typing
                    })
                    VStack {
                        Button(action: {
                            self.emailAlertText = ""
                            self.passwordAlertText = ""
                            if self.email.isEmpty{
                                self.emailAlertText = "Enter Email"

                            }else if self.email != "abc@quin.design"{
                                self.emailAlertText = "Enter Valid Email"

                            }else if self.password.isEmpty {
                                self.passwordAlertText = "Enter Password"
                            }else if self.password != "Quin123"{
                                self.passwordAlertText = "Enter Valid Password"

                            }else{
                                print(self.email)
                                print(self.password)
                                    UserDefaults.standard.setValue(self.email, forKey: "Email")
                                    UserDefaults.standard.setValue(self.password, forKey: "Password")
                                UIApplication.shared.addRootController(view: AnyView(TabBarView()))
                            }
                            
                            
                            
                        }) {
                            Text("Login").font(.system(size: 16, weight: .medium, design: .default))
                            .frame(width: 200, height: 50, alignment: .center)
                            .background(Color.gray)
                            .foregroundColor(Color.white)
                            .cornerRadius(5)
                        }
                    }

                }
            }
            .onTapGesture {
                if self.isTyping{
                    UIApplication.shared.endEditing()
                }
            }
            .onAppear(){
                //MARK:- To clear Data from CoreData
                DBManager.shared.deleteAllData()
            }
            .navigationBarTitle("Login".uppercased())
            .navigationBarHidden(true)
            
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
