//
//  MapView.swift
//  QuinTask
//
//  Created by Ali Almorshed on 14/04/21.
//

import SwiftUI
import Combine
import MapKit
import CoreLocation

struct City: Identifiable {
    let id = UUID()
    let coordinate: CLLocationCoordinate2D
}

struct MapView: View {
    @Environment(\.safeAreaInsets) private var safeAreaInsets

    @State var searchCompleter = MKLocalSearchCompleter()
    @State var locationManager = CLLocationManager()
    @State var showMapAlert = false
    @State var route: MKPolyline?
    @State var destinationAnnotation: MKPointAnnotation?
    @State var isTyping = false
    @State private var searchTerm : String = ""
    @State private var searchResults: [MKLocalSearchCompletion] = []
    @State private var searchSource: [String] = []
    @ObservedObject var locationService: LocationService

    @State private var noRouteText: String = ""


    var body: some View {
        ZStack{
            MapUIView(locationManager: $locationManager, showMapAlert: $showMapAlert, route: $route, destinationAnnotation: $destinationAnnotation)
                
            .alert(isPresented: $showMapAlert) {
              Alert(title: Text("Location access denied"),
                    message: Text("Your location is needed"),
                    primaryButton: .cancel(),
                    secondaryButton: .default(Text("Settings"),
                                              action: { self.goToDeviceSettings() }))
            }
            .onAppear() {
                if let location = iHelper.selectedLocation, let endPoint = location.destination {
                    iHelper.selectedLocation = nil
                    self.drawPolylineBetween(endPoint, location)
                    self.locationService.queryFragment = ""
                }
            }
            
            VStack {
                Spacer()
                SearchBar(placeholder: "Search Here", text: $locationService.queryFragment)
                .padding([.top], 50)
                .onReceive(Just(locationService.queryFragment)) { newValue in
                    print("came here ==",locationService.queryFragment)
                    if newValue.count > 0{
                        if self.noRouteText != ""{
                            self.noRouteText = ""
                        }
                    }
                }
                if noRouteText != ""{
                    Text(self.noRouteText)
                    .frame(height: UIScreen.height)

                }else{

                ScrollView(.vertical, showsIndicators: false, content: {
                        ForEach(locationService.searchResults, id: \.self) { completion in

                                Button(action: {
                                    let searchRequest = MKLocalSearch.Request(completion: completion)
                                    let search = MKLocalSearch(request: searchRequest)
                                    search.start { (response, error) in
    //                              if error == nil {

                                        let coordinate = response?.mapItems[0].placemark.coordinate

                                        if let endPoint = coordinate{
                                            let location = Location()
                                            location.title = completion.title
                                            location.subTitle = completion.subtitle
                                            location.destination = endPoint
                                            UIApplication.shared.endEditing()
                                            self.drawPolylineBetween(endPoint, location)
                                            self.locationService.queryFragment = ""
                                        }
    //                                }
                                    }
                                }, label: {
                                    VStack(alignment: .leading) {
                                        Text(completion.title)
                                        .foregroundColor(Color.black)
                                        .padding([.leading], 5)
                                        Text(completion.subtitle)
                                        .foregroundColor(Color.black)
                                       .font(.subheadline)
                                        .padding([.leading], 5)
                                        Color.gray.frame(height:CGFloat(1) / UIScreen.main.scale)
                                   }
                                    })

                            

                        }
                    
                })
                .clipped()
                .background(Color(.white))
                .padding([.leading, .trailing], 10)
                .cornerRadius(10)
                Spacer()
                }
            }
            .padding(safeAreaInsets)

        }
        .edgesIgnoringSafeArea(.all)
        .navigationBarHidden(true)
        .onTapGesture {
                UIApplication.shared.endEditing()
        }


    }

}
private extension MapView {

    func drawPolylineBetween(_ endPointCoordinate: CLLocationCoordinate2D,_ location: Location) {
        self.noRouteText = ""
        let locValue:CLLocationCoordinate2D = self.locationManager.location!.coordinate
        print("CURRENT LOCATION = \(locValue.latitude) \(locValue.longitude)")
        print("END LOCATION = \(endPointCoordinate.latitude) \(endPointCoordinate.longitude)")

        let start = CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude)
        
        let startPoint = MKMapItem(placemark: MKPlacemark(coordinate: start))
        let endPoint = MKMapItem(placemark: MKPlacemark(coordinate: endPointCoordinate))
        let request = MKDirections.Request()
        request.source = startPoint
        request.destination = endPoint
        request.requestsAlternateRoutes = false
        request.transportType = .automobile
        let direction = MKDirections(request: request)
        direction.calculate { (response, error) in
            if let response = response, let route = response.routes.first {
                self.route = route.polyline
                iHelper.addLocation(location)
                let destinationPin = MKPointAnnotation()
                destinationPin.coordinate = endPointCoordinate
                self.destinationAnnotation = destinationPin
            }else{
                self.route = nil
                self.destinationAnnotation = nil
                self.noRouteText = "No Route Available"
            }
        }
        
        print("Polyline drawn from \(start) to \(endPointCoordinate)")

    }
}


struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(locationService: LocationService())
    }
}

extension MapView{
    func goToDeviceSettings() {
      guard let url = URL.init(string: UIApplication.openSettingsURLString) else { return }
      UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

}




