//
//  MapUIView.swift
//  QuinTask
//
//  Created by Ali Almorshed on 15/04/21.
//

import SwiftUI
import MapKit
import CoreLocation

struct MapUIView: UIViewRepresentable {


  @Binding var locationManager: CLLocationManager
  @Binding var showMapAlert: Bool
    @Binding var route: MKPolyline?
    @Binding var destinationAnnotation: MKPointAnnotation?


  let map = MKMapView()

  ///Creating map view at startup
  func makeUIView(context: Context) -> MKMapView {
    locationManager.delegate = context.coordinator
    map.delegate = context.coordinator
    map.showsCompass = false // hides current compass, which shows only on map turning
    
    let compassBtn = MKCompassButton(mapView: map)
    compassBtn.frame.origin = CGPoint(x: UIScreen.width-(compassBtn.frame.width+10), y: UIScreen.height*0.8) // you may use GeometryReader to replace it's position
    compassBtn.compassVisibility = .visible // compass will always be on map
    map.addSubview(compassBtn)

    return map
  }

  func updateUIView(_ view: MKMapView, context: Context) {
    map.showsUserLocation = true
    map.userTrackingMode = .follow
    map.isScrollEnabled = true
    map.isRotateEnabled = true

    addRoute(to: view)

  }

  ///Use class Coordinator method
  func makeCoordinator() -> MapUIView.Coordinator {
    return Coordinator(mapView: self)
  }

  //MARK: - Core Location manager delegate
  class Coordinator: NSObject, CLLocationManagerDelegate, MKMapViewDelegate {

    var mapView: MapUIView

    init(mapView: MapUIView) {
      self.mapView = mapView
    }

    ///Switch between user location status
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
      switch status {
      case .restricted:
        break
      case .denied:
        mapView.showMapAlert.toggle()
        return
      case .notDetermined:
        mapView.locationManager.requestWhenInUseAuthorization()
        return
      case .authorizedWhenInUse:
        return
      case .authorizedAlways:
        mapView.locationManager.allowsBackgroundLocationUpdates = true
        mapView.locationManager.pausesLocationUpdatesAutomatically = false
        return
      @unknown default:
        break
      }
      mapView.locationManager.startUpdatingLocation()

    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if (overlay is MKPolyline) {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.fillColor = UIColor.red
            renderer.strokeColor = UIColor.red
            renderer.lineWidth = 2
            return renderer
        }else{
            return MKOverlayRenderer()
        }
    }

    
   }
  }
 


private extension MapUIView {
    func addRoute(to view: MKMapView) {
//        if !view.overlays.isEmpty {
//            view.removeOverlays(view.overlays)
//        }
        let overlays = view.overlays
        for overlay in overlays {
            view.removeOverlay(overlay)
        }


        if !view.annotations.isEmpty {
            view.removeAnnotations(view.annotations)
        }

        guard let route = route else { return }
        let mapRect = route.boundingMapRect
        view.setVisibleMapRect(mapRect, edgePadding: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10), animated: true)
        view.addOverlay(route)
        
        guard let annotation1 = destinationAnnotation else { return }
        view.addAnnotation(annotation1)

        
    }
}
