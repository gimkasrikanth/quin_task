//
//  Location.swift
//  QuinTask
//
//  Created by Ali Almorshed on 16/04/21.
//

import Foundation
import CoreLocation

class Location {
    
    var title: String?
    var subTitle: String?
    var destination: CLLocationCoordinate2D?

}
