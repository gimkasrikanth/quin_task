//
//  File.swift
//  QuinTask
//
//  Created by Ali Almorshed on 15/04/21.
//

import Foundation



let iHelper = Helper.sharedInstance()

class Helper: NSObject, ObservableObject {
    
    static var _Helper : Helper?
    
    class func sharedInstance() -> Helper {
        if (_Helper == nil)
        {
            _Helper = Helper()
        }
        
        return (_Helper)!
    }
    var selectedLocation: Location?
    @Published  var locationsList: [DBEntity] = DBManager.shared.fetchAllData().reversed()

    
    func addLocation(_ completion: Location){
        DBManager.shared.saveDataFor(completion)
        self.locationsList = DBManager.shared.fetchAllData().reversed()
        
    }
}
