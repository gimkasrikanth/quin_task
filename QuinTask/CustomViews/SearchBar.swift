//
//  SearchBar.swift
//  QuinTask
//
//  Created by Ali Almorshed on 14/04/21.
//

import SwiftUI


struct SearchBar: View {
    @State var placeholder: String = ""
    @Binding var text: String

    @State private var isEditing = false
        
    var body: some View {
        HStack {
            
//            TextInputView(placeholder: placeholder, text: $text)
//            .frame(height: 40)
            TextField(placeholder, text: $text)

                .padding(10)
                .padding(.horizontal, 25)
                .background(Color.white)
                .cornerRadius(25)
                .shadow(radius: 2)
                .overlay(
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.gray)
                            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                            .padding(.leading, 8)
                        
                        if isEditing {
                            Button(action: {
                                self.text = ""
                                
                            }) {
                                Image(systemName: "multiply.circle.fill")
                                    .foregroundColor(.gray)
                                    .padding(.trailing, 8)
                            }
                        }
                    }
                )
                .padding(.horizontal, 10)
                .onTapGesture {
                    print("Began Editing")
                    self.isEditing = true
                }
            
            if isEditing {
                Button(action: {
                    self.isEditing = false
                    self.text = ""
                    
                    UIApplication.shared.endEditing()
                }) {
                    Text("Cancel")
                }
                .padding(.trailing, 10)
                .transition(.move(edge: .trailing))
                .animation(.default)
            }
        }
    }
}
