//
//  CusstomTextField.swift
//  SwiftUI-Sample
//
//  Created by Ali Almorshed on 22/03/21.
//

import SwiftUI
import Combine
struct CustomTextField: View {
    @State var placeholder: String = ""
    @Binding var errorMessage: String
    @State private var text: String = ""
    @State var isTyping = false
    @State var secureText: Bool = false
    @State var inputType: TextValidator = .text
    @State var editing: ((_ text: String, _ typing: Bool)-> Void)?

    var body: some View {
        if self.secureText {
            SecureField(placeholder, text: $text, onCommit: {
                isTyping = false
                self.editing?(self.text, isTyping)
            })
            .frame(height: 50)
            .textFieldStyle(PlainTextFieldStyle())
            .padding([.leading, .trailing], 5)
            .cornerRadius(10)
            .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray))
            .padding([.leading, .trailing], 24)
            .onReceive(Just(text)) { newValue in
                isTyping = true
                var filtered = newValue.filter { "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ".contains($0) }

                if self.inputType == .number{
                    filtered = newValue.filter { "0123456789".contains($0) }
                }
                if self.inputType == .email{
                    filtered = newValue.filter { "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._0123456789".contains($0) }
                }
                if filtered != newValue {
                    self.text = filtered
                }

                self.editing?(self.text, isTyping)
            }
            HStack{
                Text(self.errorMessage)
                    .font(.subheadline)
                    .foregroundColor(Color.red)
                Spacer()
            }
            .padding([.leading, .trailing], 24)
        }else{
            TextField(placeholder, text: $text, onEditingChanged: { (isBegin) in
                isTyping = isBegin
                self.editing?(self.text, isTyping)
            },
            onCommit: {
                isTyping = false
                self.editing?(self.text, isTyping)
            }
            )
            .frame(height: 50)
            .textFieldStyle(PlainTextFieldStyle())
            .padding([.leading, .trailing], 5)
            .cornerRadius(10)
            .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray))
            .padding([.leading, .trailing], 24)
            .onReceive(Just(text)) { newValue in
                var filtered = newValue.filter { "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ".contains($0) }

                if self.inputType == .number{
                    filtered = newValue.filter { "0123456789".contains($0) }
                }
                if self.inputType == .email{
                    filtered = newValue.filter { "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._0123456789@".contains($0) }
                }
                if filtered != newValue {
                    self.text = filtered
                }

                self.editing?(self.text, isTyping)
            }
            HStack{
                Text(self.errorMessage)
                    .font(.subheadline)
                    .foregroundColor(Color.red)
                Spacer()
            }
            .padding([.leading, .trailing], 24)
        }
    }
}



enum TextValidator {
    case number
    case text
    case email
}
