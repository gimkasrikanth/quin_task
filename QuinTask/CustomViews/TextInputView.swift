//
//  TextInputView.swift
//  QuinTask
//
//  Created by Ali Almorshed on 16/04/21.
//

import Foundation
import SwiftUI
import Combine

struct TextInputView : UIViewRepresentable {

    

    @State var placeholder: String = ""
    @Binding var text : String

    class Cordinator : NSObject, UITextFieldDelegate {

        @Binding var text : String


        init(text : Binding<String>) {
            _text = text
        }

        func textFieldDidBeginEditing(_ textField: UITextField) {
            text = textField.text ?? ""
        }
        func textFieldDidEndEditing(_ textField: UITextField) {
            text = textField.text ?? ""
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }

    }

    func makeCoordinator() -> TextInputView.Cordinator {
        return Cordinator(text: $text)
    }

    func makeUIView(context: Context) -> UITextField {
        let searchBar = UITextField(frame: .zero)
        searchBar.placeholder = placeholder

        searchBar.delegate = context.coordinator
        return searchBar
    }

    func updateUIView(_ uiView: UITextField, context: Context) {
        uiView.text = text
    }
}
