//
//  TabButton.swift
//  QuinTask
//
//  Created by Ali Almorshed on 14/04/21.
//

import SwiftUI

struct TabButton: View {
    
    var id : TabName
    var title : String
    var imageName : String
    @Binding var selectedTab : TabName
    var animation : Namespace.ID
    var viewWidth : CGFloat = 0

    var body: some View{
        
        Button(action: {
            withAnimation{selectedTab = id}
        }) {
            
            VStack(spacing: 6){
                
                // Top Indicator....
                
                // Custom Shape...
                
                // Slide in and out animation...
                
                ZStack{
                    
                    CustomShape()
                        .fill(Color.clear)
                        .frame(width: self.viewWidth, height: 6)
                    
                    if selectedTab == id{
                        
                        CustomShape()
                            .fill(Color(.red))
                            .frame(width: self.viewWidth, height: 6)
                            .matchedGeometryEffect(id: "Tab_Change", in: animation)
                    }
                }
                .padding(.bottom,10)
                
                Image(systemName: imageName)
                    .renderingMode(.template)
                    .resizable()
                    .foregroundColor(selectedTab == id ? Color(.red) : Color.black.opacity(0.2))
                    .frame(width: 24, height: 24)
                
                Text(title)
                    .font(.caption)
                    .fontWeight(.bold)
                    .foregroundColor(Color.black.opacity(selectedTab == id ? 0.6 : 0.2))
            }
        }
    }
}


