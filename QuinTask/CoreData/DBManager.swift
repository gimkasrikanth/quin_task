//
//  DBManager.swift
//  QuinTask
//
//  Created by Ali Almorshed on 15/04/21.
//

import Foundation
import UIKit
import CoreData
import MapKit
import CoreLocation

class DBManager {
    public static let shared = DBManager()
    var entityData: [DBEntity] = []
 
    func saveDataFor(_ location: Location){
        if !self.checkAvailability(location){
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            
            let userEntity = NSEntityDescription.entity(forEntityName: "DBEntity", in: managedContext)!
            
            
            if let destination = location.destination {
                let user = NSManagedObject(entity: userEntity, insertInto: managedContext)
                user.setValue(location.title, forKeyPath: "title")
                user.setValue(location.subTitle, forKey: "subTitle")
                user.setValue(destination.latitude, forKey: "latitude")
                user.setValue(destination.longitude, forKey: "longitude")
            }

            
            do {
                try managedContext.save()
               
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    
    func retrieveData() {

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "DBEntity")
        
        do {
            if let result = try managedContext.fetch(fetchRequest) as? [DBEntity]{
                entityData = result
            }
//            let result = try managedContext.fetch(fetchRequest)
//            for data in result as! [NSManagedObject] {
//                print(data.value(forKey: "title") as! String)
//            }
            
        } catch {
            
            print("Failed")
        }
    }
    func checkAvailability(_ location: Location) -> Bool{
        if let destination = location.destination {

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return false}
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "DBEntity")
            fetchRequest.predicate = NSPredicate(format: "latitude = %@ AND longitude = %@", argumentArray: [destination.latitude, destination.longitude])

        do
        {
            let test = try managedContext.fetch(fetchRequest)
            if test.count == 1 {
                return true
            }
            
            }
        catch
        {
            print(error)
        }
        }
        
        return false
    }
    func updateData(_ location: Location){
        if let destination = location.destination {

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "DBEntity")
//            fetchRequest.predicate = NSPredicate(format: "latitude = %@", destination.latitude)
            fetchRequest.predicate = NSPredicate(format: "latitude = %@ AND longitude = %@", argumentArray: [destination.latitude, destination.longitude])

        do
        {
            let test = try managedContext.fetch(fetchRequest)
   
                let objectUpdate = test[0] as! NSManagedObject
            objectUpdate.setValue(location.title, forKeyPath: "title")
            objectUpdate.setValue(location.subTitle, forKey: "subTitle")
            objectUpdate.setValue(destination.latitude, forKey: "latitude")
            objectUpdate.setValue(destination.longitude, forKey: "longitude")
                do{
                    try managedContext.save()
                }
                catch
                {
                    print(error)
                }
            }
        catch
        {
            print(error)
        }
        }
    }
    
     func deleteData(_ location: Location){
        if let destination = location.destination {

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        

        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "DBEntity")
        fetchRequest.predicate = NSPredicate(format: "latitude = %@", destination.latitude)
       
        do
        {
            let test = try managedContext.fetch(fetchRequest)
            
            let objectToDelete = test[0] as! NSManagedObject
            managedContext.delete(objectToDelete)
            
            do{
                try managedContext.save()
            }
            catch
            {
                print(error)
            }
            
        }
        catch
        {
            print(error)
        }
        }
    }
    func fetchAllData() -> [DBEntity] {
        
        self.retrieveData()
        return self.entityData
    }
    
    func deleteAllData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "DBEntity")
        fetchRequest.returnsObjectsAsFaults = false

        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.delete(managedObjectData)
            }
        } catch let error as NSError {
            print("Detele all data in DBEntity error : \(error) \(error.userInfo)")
        }

        _ = fetchAllData()
    }

}
